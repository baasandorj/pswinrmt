
<#
.SYNOPSIS
    Get all installed applications list or uninstall an application
.DESCRIPTION
    This script collects all installed applications list or uninstalls an application from windows operating system
.EXAMPLE
    .\Get-InstalledApps.ps1
    .\Get-InstalledApps.ps1 -A "All"
    .\Get-InstalledApps.ps1 -A "All" -Action List
    .\Get-InstalledApps.ps1 -A "All" -Action List -ResultFile .\All-Installed-Application.txt
    To get all installed applications information and saved it into ".\All-Installed-Application.txt" file. 
.EXAMPLE
    .\Get-InstalledApps.ps1 -A "Python"
    .\Get-InstalledApps.ps1 -A "Python" -Action List
    Get installed Python application information 
.EXAMPLE
    .\Get-InstalledApps.ps1 -A "PYTHON 2.7.17 (64-BIT)" -Action Uninstall
    To uninstall "PYTHON 2.7.17 (64-BIT)" application with " /norestart /quiet /passive" 

.INPUTS
    Inputs (if any)
.OUTPUTS
    Output (if any)
.NOTES
    General notes
#>

[CmdletBinding()]
param (
    [Parameter(HelpMessage = "Enter Application name or All for getting installed application information.")]
    [Alias("Application","A")]
    [String]
    $ApplicationName = "All",

    [Parameter(HelpMessage = "Enter results save file name.")]
    [Alias("Output","Result","R")]
    [String]
    $ResultFile = ".\All-Installed-Application.txt",

    [Parameter(HelpMessage = "Enter action information list or uninstall ")]
    [ValidateSet("List","Uninstall")]
    [String]
    $Action = "List"
)

Function Get-Application {
    param($ApplicationName)
    # The register location 1
    $Applications="\SOFTWARE\Microsoft\Windows\CurrentVersion\Installer\UserData\S-1-5-18\Products" 
    $Information=get-childitem -path "HKLM:$Applications" | select-object -expandproperty pschildname
    $Information | ForEach-Object {    
        $SourcePath="HKLM:$Applications\$_\InstallProperties"
        if (Test-Path $SourcePath) {
            $AllNeededInformation=Get-ItemProperty -path $SourcePath | select-object Displayname,DisplayVersion,InstallDate,InstallLocation,InstallSource,Publisher,UninstallString
            if ($AllNeededInformation.Displayname -like "*$ApplicationName*") {
               $AllNeededInformation
            }
        }
    }
    # The register location 2
    $Applications="\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"
    $Information=get-childitem -path "HKLM:$Applications" | select-object -expandproperty pschildname
    $Information | ForEach-Object {    
        $SourcePath="HKLM:$Applications\$_"
        if (Test-Path $SourcePath) {
            $AllNeededInformation=Get-ItemProperty -path $SourcePath | select-object @{name="Displayname";e={($_.Displayname).ToUpper()}},DisplayVersion,InstallDate,InstallLocation,InstallSource,Publisher,UninstallString
            if ($AllNeededInformation.Displayname -like "*$ApplicationName*") { 
               $AllNeededInformation
            }
        }
    }
}
Function Uninstall-Application {
    param($ApplicationName)
    Write-Host "Uninstalling $ApplicationName"
    if (-not $ApplicationName.Contains("*")) {
        $AllNeededInformation=Get-Application $ApplicationName | Sort-Object -Property Displayname -Unique | Select-Object -First 1
        if ($AllNeededInformation.Displayname -like "*$ApplicationName*") {
            if ($AllNeededInformation.UninstallString) {
                $c="$($AllNeededInformation.UninstallString) /norestart /quiet /passive"
                Write-Host "Uninstalling it by $c command..."
                cmd /c "$c && echo 'Completed!' || echo 'Failed!'"
                Write-Host "$ApplicationName uninstalling has been completed!"
            }
        } else {
            Write-Error "Installation of $ApplicationName not found!"   
        }
    }
}

if (Test-Path "$ResultFile") { Remove-Item $ResultFile -Force | Out-Null }

if ($Action -eq "List") {
    if ($ApplicationName -eq "All") {
        Get-Application "*" | Sort-Object -Property Displayname -Unique | Export-Csv -Delimiter "`t" $ResultFile -NoTypeInformation
        Write-Output "All installed applications list has been saved into the $ResultFile file!"
    } else {
        $Result=Get-Application "*$ApplicationName*" | Sort-Object -Property Displayname -Unique
        if ($Result) {
            $Result
        } else {
            Write-Error "Installation of $ApplicationName not found!"
        }
    }
}

if ($Action -eq "Uninstall") {
    Write-Output "$ApplicationName"
    if (($ApplicationName -ne "All") -and ($ApplicationName -ne "*"))  {
        Uninstall-Application $ApplicationName
    }
}