<#
.SYNOPSIS
    Find Top CPU Spike process in Windows Operating System
.DESCRIPTION
    This script displays and records of spike starting and ending time, name and CPU utilization 
    parameters of the top processes when the CPU utilized more than $Percent on Windows OS system.
    Written Baasandorj Namnansuren
    CreatedDate 3/2/2020
    LastUpdatDate 3/6/2020
.EXAMPLE
    PS C:\> .\Find-HighCPUConsumingProcess.ps1 -Percent 50 -ResultFile c:\temp\result-high-cpu-spike.txt -SelectedProcessNumbers 5
    Set the CPU Threshold to 50% and Results would be saved to c:\temp\result-high-cpu-spike.txt file for future review.
.INPUTS
    Inputs (if any)
.OUTPUTS
    Output (if any)
.NOTES
    General notes
#>

[CmdletBinding()]
param (
    [Parameter(HelpMessage = "Enter threshold value of CPU.")]
    [Alias("Threshold","T")]
    [int]
    $Percent = 70,

    [Parameter(HelpMessage = "Enter results save file name.")]
    [Alias("Output","Result","R")]
    [String]
    $ResultFile = ".\result-high-cpu-spike.txt",

    [Parameter(HelpMessage="Enter how many prcoccses showing.")]
    [Int]
    $SelectedProcessNumbers = 5
)

$Cycle = 0
$LastTopProcessName = ""
$CpuCores = (Get-WMIObject Win32_ComputerSystem).NumberOfLogicalProcessors

if (-not (Test-Path $ResultFile)) {
    New-Item -Name $ResultFile -Force | Out-Null
}

do {
    do {
        $CounterSamples = (Get-Counter -Counter "\Processor(_Total)\% Processor Time" -SampleInterval 2 -MaxSamples 1).CounterSamples
        if ( $LastTopProcessName -ne "" -and $CounterSamples.CookedValue -le $Percent ) { 
            Write-Output "Finish $(Get-Date -format G)" | Tee-Object $ResultFile -Append
            Write-Output "." | Tee-Object $ResultFile -Append
            $LastTopProcessName = ""
        }
    } while ( $CounterSamples.CookedValue -le $Percent )

    $Processes = (Get-Counter '\Process(*)\% Processor Time' -ErrorAction SilentlyContinue ).CounterSamples

    $CurrentTopNProcess = $Processes | Where-Object { ( $_.Instancename -ne 'idle' -and $_.Instancename -ne '_total' <#-and $_.CookedValue -gt 10#> ) } `
                         | Sort-Object CookedValue -Descending | Select-Object -First $SelectedProcessNumbers
    $CurrentTopProcess = $CurrentTopNProcess | Select-Object -First 1

    if ($CurrentTopProcess.Instancename -ne $LastTopProcessName) {
        Write-Output "Start $(Get-Date -format G)" | Tee-Object $ResultFile -Append
        ($CurrentTopNProcess | Select-Object Instancename,@{ N="CPU %";Expression={ "{0:N2}" -f ($_.CookedValue / $CpuCores ) } } | `
                              Format-Table -AutoSize | Out-String).Trim() | Tee-Object $ResultFile -Append
        $LastTopProcessName = $CurrentTopProcess.Instancename
    }
    # Ctrl + C is stop
    if ($Host.UI.RawUI.KeyAvailable -and ($Key = $Host.UI.RawUI.ReadKey("AllowCtrlC,NoEcho,IncludeKeyUp"))) {
        If ([Int]$Key.Character -eq 3) {
            $Cycle = 1
        }
    }
} while ($Cycle -eq 0)
