Function Get-RebootTime {
    param($EventId)
    $StartTime= (get-date).AddDays(-30)
    Return Get-WinEvent -FilterHashtable @{Logname="System"; id=$EventId; StartTime=$StartTime} -ErrorAction SilentlyContinue | Select-Object -First 1  | Select-Object TimeCreated,Message
}

$RebootInformation = Get-RebootTime -EventId 1074
if ($RebootInformation) {
    $RebootInformation | Format-List
} else {
    Get-RebootTime -EventId 41 | Format-List
    Get-RebootTime -EventId 6006 | Format-List
    Get-RebootTime -EventId 6005 | Format-List
}

