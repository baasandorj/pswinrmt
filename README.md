This project maintains Powershell scripting tools for automating daily Windows Systems Administrator tasks  

- Find-HighCPUConsumingProcess.ps1 does find what process consumes top CPU resources.
- Get-InstalledApps does collect installed windows applications list and runs much faster than WMIC PRODUCT LIST ... command. 

More coming ...

- disk info
- find a text in the windows logs such as service start/stop, user logged in, last reboot time etc
- ...
