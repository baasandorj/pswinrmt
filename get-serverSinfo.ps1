# This script helps collecting domain members compter information
# Powershell Active directory module is required
# Written Baasandorj Namnansuren

# Test server connection
try {
    $ServerCredential = Get-Credential $(whoami)
    $DomainName= New-Object System.DirectoryServices.DirectoryEntry(("LDAP://"+[ADSI]'').Distinguishedname,$ServerCredential.UserName,$ServerCredential.GetNetworkCredential().Password)
} catch {
    $_.ExceptionMessage
    continue 
}
if ($null -ne $Domainname.name) {
    (get-adcomputer -Filter *) | ForEach-Object {
        $ServerName = $_.Name
        # Test-WSMan -ComputerName $ServerName -Authentication Default
        Invoke-Command -ComputerName $ServerName -Authentication Default -ScriptBlock {
            # Computer and OS 
            $OS=Get-CimInstance -ComputerName $ServerName -Class CIM_OperatingSystem | Select-Object Caption,OSArchitecture,InstallDate,LastBootUpTime,PortableOperatingSystem 
            $CPU=Get-CimInstance -ComputerName $ServerName -Class CIM_Processor | Select-Object Name,MaxClockSpeed,Caption,NumberOfCores,NumberofLogicalProcessors 
            Get-CimInstance -ComputerName $ServerName -Class CIM_ComputerSystem | Select-Object Name,Manufacturer,Model,SystemType, `
                            @{Name="Installed RAM";Expression={"{0:N0} Gb" -f ($_.TotalPhysicalMemory/1GB)}}, `
                            @{Name="SerialNumber";Expression={Get-WmiObject -Class Win32_Bios | Select-Object -ExpandProperty SerialNumber}}, `
                            @{Name="Windows Version";Expression={$OS.Caption}}, `
                            @{Name="OS Bit";Expression={$OS.OSArchitecture}}, `
                            @{Name="Install date";Expression={$OS.InstallDate}}, `
                            @{Name="Last Reboot Time";Expression={$OS.LastBootUpTime}}, `
                            @{Name="Portable";Expression={$OS.PortableOperatingSystem}}, `
                            @{Name="Processor";Expression={$CPU.Name}}, `
                            #@{Name="CPU Speed";Expression={$CPU.MaxClockSpeed/1000}}, `
                            @{Name="CPU Cores Number";Expression={$CPU.NumberOfCores}}, `
                            @{Name="CPU Logic Processers Number";Expression={$CPU.NumberofLogicalProcessors}}

            # GPU
            Get-CimInstance -ComputerName $ServerName -Class CIM_VideoController | Format-Table -Property Name,DeviceID,AdapterRAM,VideoProcessor 

            # Memory
            Get-CimInstance -ComputerName $ServerName -Class CIM_PhysicalMemory | Format-Table Manufacturer,BankLabel,Capacity,@{Name="GB";Expression={"{0:N0}Gb" -f ($_.Capacity/1Gb)}}, DeviceLocator, PartNumber, SerialNumber, PositionInRow, Speed, Tag 
            #(get-wmiobject -ComputerName $ServerName -class Win32_Physicalmemory | Measure-Object -Property Capacity  -Sum ).Sum /1GB
            Get-CimInstance -ComputerName $ServerName -Class CIM_PhysicalMemory | Format-Table Manufacturer,BankLabel,@{Name="Size";Expression={"{0:N0}Gb" -f ($_.Capacity/1Gb)}}, DeviceLocator, PartNumber, SerialNumber, PositionInRow, Speed, Tag 
            Get-CimInstance -ComputerName $ServerName -Class CIM_PhysicalMemory | Format-Table Manufacturer,BankLabel,Capacity, DeviceLocator, PartNumber, SerialNumber, PositionInRow, Speed, Tag 

            # Disk
            #Get-CimInstance -Class CIM_LogicalDisk | select-Object Name,Size,Freespace | Format-Table *
            Get-CimInstance -ComputerName $ServerName -Class CIM_LogicalDisk | Format-Table -Property Name,@{Name="Disk Size";Expression={"{0,15:N2} Gb" -f ($_.Size/1Gb)};Align="Right"},@{Name="Free Space";Expression={"{0,15:N2} Gb" -f ($_.FreeSpace/1Gb)};Align="Right"} 
            Get-CimInstance -ComputerName $ServerName -Class CIM_DiskDrive |  Format-Table -Property Name,Index,InterfaceType,Size,Caption,Description,MediaType,Model,SerialNumber 
            Get-CimInstance -ComputerName $ServerName -Class Win32_CDROMDrive | Format-Table -Property Name,Drive,Caption,DeviceID,Manufacturer 

            # Network
            #Get-WmiObject -Class Win32_NetWorkAdapter | Select-Object AdapterType,MACAddress,Manufacturer,Speed | Format-Table
            Get-CimInstance -ComputerName $ServerName -Class Win32_NetworkAdapterConfiguration | Select-Object Caption,IPAddress,IPSubnet,DefaultIPGateway,DNSServerSearchOrder,MacAddress,DHCPEnabled,DHCPServer | Format-Table
        }
    }
} else {
    Write-Output "You have entered wrong password!"
}    

